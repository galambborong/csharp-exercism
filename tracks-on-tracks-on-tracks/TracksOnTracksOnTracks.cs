using System.Collections.Generic;
using System.Linq;

namespace TracksOnTracksOnTracks;

public static class Languages
{
    private const string CSharpLanguage = "C#";
    public static List<string> NewList() => new();

    public static List<string> GetExistingLanguages() => new() { CSharpLanguage, "Clojure", "Elm" };

    public static List<string> AddLanguage(List<string> languages, string language)
    {
        languages.Add(language);
        return languages;
    }

    public static int CountLanguages(List<string> languages) => languages.Count;

    public static bool HasLanguage(List<string> languages, string language = CSharpLanguage) => languages.Contains(language);

    public static List<string> ReverseList(List<string> languages)
    {
        languages.Reverse();
        return languages;
    }

    public static bool IsExciting(List<string> languages) => languages switch
    {
        _ when HasLanguage(languages) => languages switch
        {
            _ when CountLanguages(languages) is 2 or 3 => true,
            _ when languages.First() == CSharpLanguage => true,
            _ => false
        },
        _ => false
    };

    public static List<string> RemoveLanguage(List<string> languages, string language)
    {
        languages.Remove(language);
        return languages;
    }

    public static bool IsUnique(List<string> languages) => languages.Distinct().Count() == languages.Count;
}