abstract class Character
{
    private readonly string _characterType;

    protected Character(string characterType)
    {
        _characterType = characterType;
    }

    public abstract int DamagePoints(Character target);

    public virtual bool Vulnerable() => false;

    public override string ToString() => $"Character is a {_characterType}";
}

class Warrior : Character
{
    public Warrior() : base("Warrior")
    {
    }

    public override int DamagePoints(Character target)
    {
        return target.Vulnerable() ? 10 : 6;
    }
}

class Wizard : Character
{
    private bool IsSpellPrepared { get; set; }

    public Wizard() : base("Wizard")
    {
    }

    public override int DamagePoints(Character target)
    {
        if (!IsSpellPrepared)
            return 3;

        IsSpellPrepared = false;
        return 12;
    }

    public void PrepareSpell() => IsSpellPrepared = true;

    public override bool Vulnerable() => !IsSpellPrepared;
}