namespace NeedForSpeed;

public class RemoteControlCar
{
    public RemoteControlCar(int speed, int batteryDrain)
    {
        Speed = speed;
        BatteryDrain = batteryDrain;
    }

    private int Odometer { get; set; }
    public int Speed { get; }
    public int BatteryDrain { get; }
    public int BatteryLife { get; private set; } = 100;

    public bool BatteryDrained() => BatteryDrain > BatteryLife;
    public int DistanceDriven() => Odometer;

    public void Drive()
    {
        if (BatteryDrained())
            return;

        Odometer += Speed;
        BatteryLife -= BatteryDrain;
    }

    public static RemoteControlCar Nitro() => new(50, 4);
}

public class RaceTrack
{
    private readonly int _distance;

    public RaceTrack(int distance)
    {
        _distance = distance;
    }

    public bool TryFinishTrack(RemoteControlCar car)
    {
        var driveUnits = _distance / car.Speed;
        var excessDistance = _distance % car.Speed;
        var batteryRemainingAtEndOfTrack = car.BatteryLife / driveUnits - excessDistance;

        return batteryRemainingAtEndOfTrack >= car.BatteryDrain;
    }
}