using System;

public static class TelemetryBuffer
{
    private const int MaxByteSize = 256;
    public static byte[] ToBuffer(long reading)
    {
        var buffer = new byte[9];
        var payloadWidth = GetPayloadWidth(reading);
        var payloadBytes = BitConverter.GetBytes(reading);
        var prefix = SetPrefix(reading, payloadWidth);

        buffer.SetValue(prefix, 0);
        Array.Resize(ref payloadBytes, payloadWidth);
        payloadBytes.CopyTo(buffer, 1);
        
        return buffer;
    }


    public static long FromBuffer(byte[] buffer)
    {
        var prefix = GetPrefix(buffer);
        return prefix switch
        {
            MaxByteSize - 8 or 4 or 2 => BitConverter.ToInt64(buffer, 1),
            MaxByteSize - 4 => BitConverter.ToInt32(buffer, 1),
            MaxByteSize - 2 => BitConverter.ToInt16(buffer, 1),
            _ => 0L
        };
    }

    private static int GetPayloadWidth(long reading) => reading switch
    {
        < int.MinValue => sizeof(long),
        < short.MinValue => sizeof(int),
        <= ushort.MaxValue => sizeof(short),
        <= uint.MaxValue => sizeof(int),
        _ => sizeof(long)
    };

    private static byte SetPrefix(long reading, int payloadWidth)
    {
        // TODO - Simplify switch arms!!
        var prefix = reading switch
        {
            0 => BitConverter.GetBytes(payloadWidth),
            >= int.MinValue and < short.MinValue => BitConverter.GetBytes(MaxByteSize - payloadWidth),
            >= short.MinValue and < 0 => BitConverter.GetBytes(MaxByteSize - payloadWidth),
            >= 0 and <= ushort.MaxValue => BitConverter.GetBytes(payloadWidth),
            > ushort.MaxValue and <= int.MaxValue => BitConverter.GetBytes(MaxByteSize - payloadWidth),
            > int.MinValue and <= uint.MaxValue => BitConverter.GetBytes(payloadWidth),
            _ => BitConverter.GetBytes(MaxByteSize - payloadWidth)
        };
        return prefix[0];
    }

    private static int GetPrefix(byte[] bytes) => bytes[0];
}